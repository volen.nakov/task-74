import "./App.css";
import { useState, useMemo, memo } from "react";

function App() {
  const [text, setText] = useState('')
  const regex = /^[0-9]+$/;
  function checkNumber(input) {
    if (regex.exec(input) != null) {
      return ("fas fa-check");
    }
    else {
      return ("fas fa-times");
    }
  }
  const Class = useMemo(() => checkNumber(text), [text])
  return (
    <div className="App">
      <div className="control has-icons-right">
        <input
          className="input is-large"
          type="text"
          placeholder="Enter number..."
          onChange={(e) => { setText(e.target.value) }}
        />
        <span className="icon is-small is-right">
          <i className={Class} />
        </span>
      </div>
    </div>
  );
}

export default App;
